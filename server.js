// Imports
const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');

const apiRouter = require('./App/apiRouter').router;

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const environnement = require('./config/config').ENVIRONNEMENT;
const config_server = require('./config/config').CONFIG_SERVER[environnement];

const clientManager = require('./App/services/chatroom/ClientManager');

const handleRegister = require('./App/services/chatroom/handlers').handleRegister;
const handleMessage = require('./App/services/chatroom/handlers').handleMessage;
const handleDisconnect = require('./App/services/chatroom/handlers').handleDisconnect;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/api/', apiRouter);

io.on('connection', function (client) {
  client.on('register', (userId) => handleRegister(userId, clientManager, client));
  client.on('message', (receive) => handleMessage(receive, clientManager));
  client.on('disconnect', () => handleDisconnect(clientManager, client));
});

server.listen(config_server.port, config_server.host, () => console.log(`Listening on host ${config_server.host} and on port ${config_server.port}`));