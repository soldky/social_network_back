const ENVIRONNEMENT = "dev";

const JWT_SIGN_SECRET = "hljhk5667vjghg67jhhjg698bhkbhkgF6sH65GttY6yYUYGy8gvf098uygt82T63";

const JWT_EXPIRED = "1h";

const CONFIG_DATABASE = {
  host: 'cluster0-lcpy3.mongodb.net',
  database_name: 'Social_Network'
};

const CONFIG_EMAIL = {
  email_sender: "admin.social_network@gmail.com",
  username: '', // email pour envoyer le mot de passe réinitialisé
  password: '' // Mot de passe de cet email
};

const CONFIG_SERVER = {
  dev: {
    host: "localhost",
    port: 5000
  }
};

module.exports = { ENVIRONNEMENT, JWT_SIGN_SECRET, JWT_EXPIRED, CONFIG_DATABASE, CONFIG_EMAIL, CONFIG_SERVER };