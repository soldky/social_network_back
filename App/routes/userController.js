// Imports
const database = require('../controllers/database/Database');

// Verificators
const emailVerificator = require('../utils/verificators/email_verificator.utils');

// Services
const routeGuard = require('../services/routeGuard/routeGuard');

// Routes
module.exports = {
  getUser: function(req, res) {

    const userId = req.body.userId;

    if(!userId) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else {
      database.get_user(userId, res);
    }

  },

  register: function(req, res) {

    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    if (!name.lastname || !name.firstname || !email || !password) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else if (!emailVerificator(email)) {
      res.status(400).json({
        "text": "Email incorrect."
      });
    }
    else {
      database.register({name, email, password}, res);
    }
  },

  login: function(req, res) {
    const { email, password } = req.body;

    if (!email || !password) {
      res.status(400).json({
          "text": "Email ou mot de passe manquant"
      });
    }
    else {
      database.login(email, password, res);
    }
  },

  resetPassword: function(req, res) {
    const { email } = req.body;

    if (!email) {
      res.status(400).json({
          "text": "Email manquant"
      });
    }
    else {
      database.reset_password(email, res);
    }
  },

  getUserProfile: function(req, res) {
    userId = routeGuard(req, res);

    if(userId) {
      database.get_user(userId, res);
    }
  },

  add_one_friend: function(req, res) {
    userId = routeGuard(req, res);
    friendId = req.body.friendId;

    if(!friendId) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else {
      database.add_one_friend({friendId, userId}, res);
    }
  },

  remove_one_friend: function (req, res) {
    userId = routeGuard(req, res);
    friendId = req.body.friendId;

    if(!friendId) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else {
      database.remove_one_friend({friendId, userId}, res);
    }
  }
};