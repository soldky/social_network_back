// Imports
const database = require('../controllers/database/Database');

// Verificators
const emailVerificator = require('../utils/verificators/email_verificator.utils');

// Services
const routeGuard = require('../services/routeGuard/routeGuard');

// Routes
module.exports = {
  getMessages: function(req, res) {
    const talkId = req.body.talkId;

    if(!talkId) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else {
      database.get_messages(talkId, res);
    }

  },

  addMessage: function(req, res) {
    const text = req.body.message.text;
    const from = req.body.message.from;
    const talk_id = req.body.talk_id;

    if(!text || !from || !talk_id) {
      res.status(400).json({
        "text": "Paramètres manquant."
      });
    }
    else {
      database.add_message({text, from, talk_id}, res);
    }
  }
};