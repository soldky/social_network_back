// Retrieve
const mongoose = require('mongoose');
const User = require('../../models/User.model');
const Talk = require('../../models/Talk.model');
const Message = require('../../models/Message.model');
const passwordHash = require("password-hash");

// Callbacks
const login_callback = require("../../callbacks/login.callback");
const register_callback = require("../../callbacks/register.callback");
const reset_password_callback = require("../../callbacks/reset_password.callback");
const get_user_callback = require("../../callbacks/get_user.callback");
const get_messages_callback = require("../../callbacks/get_messages.callback");
const new_message_callback = require("../../callbacks/new_message.callback");
const add_one_friend_callback = require("../../callbacks/add_one_friend.callback");
const remove_one_friend_callback = require("../../callbacks/remove_one_friend.callback");

// Info database
const config_database = require("../../../config/config").CONFIG_DATABASE;
const database_url = `mongodb+srv://root:exemple@${config_database.host}/${config_database.database_name}`;

class Database {
  constructor() {
    console.log(database_url);
    this.db_instance = mongoose.connect(database_url, {reconnectTries: Number.MAX_VALUE, poolSize: 10, authSource: "admin"}).then(
      () => {
        console.info("Connexion réussi");
      },
      (err) => {
        console.error(err);
      }
    );
  }

  register({name, email, password}, res) {
    const passwordUnencrypt = password;

    password = passwordHash.generate(password);

    let newUser = new User({name, email, password});

    newUser.save(
      (error, user) => register_callback(error, user, passwordUnencrypt, res)
    );
  }

  login(email, password, res) {
    User
      .findOne({ email: email })
      .populate("talks")
      .exec(function (error, user) {
        login_callback(error, user, password, res);
      });
  }

  reset_password(email, res) {
    User.findOne(
      { email: email },
      ( error, user ) => reset_password_callback(error, user, res)
    );
  }

  get_user(userId, res) {
    User
      .findOne({ _id: userId })
      .select("-password")
      .select("-talks")
      .exec(function(error, user) {
        get_user_callback(error, user, res)
      });
  }

  get_messages(talkId, res) {
    Talk
      .findOne({_id: talkId})
      .populate("messages")
      .exec(function(error, talk) {
        get_messages_callback(error, talk, res)
      });
  }

  add_message({text, from, talk_id}, res) {
    let newMessage = new Message({text, from});
    newMessage.save();
    new_message_callback(newMessage, res, talk_id);
  }

  add_one_friend({friendId, userId}, res) {
    User
      .findOneAndUpdate({ _id: userId }, { $push: { "friends": friendId } })
      .exec(function (error, user) {
        add_one_friend_callback(error, res, user, friendId);
      });
  }

  remove_one_friend({friendId, userId}, res) {
    User
      .findOneAndUpdate({ _id: userId }, { $pull: { "friends": friendId } })
      .exec(function (error, user) {
        remove_one_friend_callback(error, res, user, friendId);
      });
  }
}

const database = new Database();
module.exports = database;
