// Imports
const express = require("express");
const userController = require("./routes/userController");
const talkController = require("./routes/talkController");

// Router
exports.router = (function() {
  const apiRouter = express.Router();

  // Users routes
  apiRouter.route('/users/').post(userController.getUser);
  apiRouter.route('/users/register/').post(userController.register);
  apiRouter.route('/users/login/').post(userController.login);
  apiRouter.route('/users/reset/password').post(userController.resetPassword);
  apiRouter.route('/users/me/').get(userController.getUserProfile);
  apiRouter.route('/users/friends/add_one/').post(userController.add_one_friend);
  apiRouter.route('/users/friends/remove_one/').post(userController.remove_one_friend);

  // Talks routes
  apiRouter.route('/talks/messages/').post(talkController.getMessages);
  apiRouter.route('/talks/message/new').post(talkController.addMessage);

  return apiRouter;
})();