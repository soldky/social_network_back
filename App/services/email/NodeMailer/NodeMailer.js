const nodemailer = require('nodemailer');
const config_email = require('../../../../config/config').CONFIG_EMAIL;

class NodeMailer {

  constructor() {
    this.service = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: config_email.username,
        pass: config_email.password
      }
    });
  }

  send(to, subject, text) {
    let email_options = {
      from: config_email.email_sender,
      to: to,
      subject: subject,
      text: text
    };

    return new Promise((resolve, reject) => {
      this.service.sendMail(email_options, function (error, info) {
        if (error) {
          reject(error);
        } else {
          resolve('Email sent: ' + info.response);
        }
      });
    });
  }

}

let instance = new NodeMailer();

module.exports = instance;