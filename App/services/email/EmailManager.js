const instance = require('./NodeMailer/NodeMailer');

class EmailManager {
  static send(to, subject, text) {
    return instance.send(to, subject, text);
  }
}

module.exports = EmailManager;