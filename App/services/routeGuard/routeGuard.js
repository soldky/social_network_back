// Utils
const authentificate = require('../../utils/user.utils').authentificate;

function routeGuard(req, res) {
  const headerAuth = req.headers['authorization'];

  try {
    if (headerAuth) {
      return authentificate(headerAuth);
    }
    else {
      res.status(401).json({
        "text": "Authentification nécessaire."
      });
      return false;
    }
  }
  catch (error) {
    res.status(401).json({
      "text": "Token invalide."
    });
    return false;
  }
}

module.exports = routeGuard;