const database = require('../../controllers/database/Database');

function handleRegister(userId, clientManager, client) {
  clientManager.addClient(userId, client);
}

function handleMessage({from, to, message}, clientManager) {

  toClient = clientManager.getClient(to);

  if(toClient) {
    toClient = toClient.client;
    toClient.emit('message', {message, from});
  }
  else {
    console.log("deconnecté");
  }
}

function handleDisconnect(clientManager, client) {
  // remove user profile
  clientManager.removeClient(client);
  console.log('client disconnect...', client.id);
}

module.exports = { handleRegister, handleMessage, handleDisconnect };
