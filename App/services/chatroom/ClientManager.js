let clients = new Map();

function addClient(userId, client) {
  console.log('client connected...', client.id);
  clients.set(userId, { client });
}

function getClient(userId) {
  return clients.get(userId);
}

function removeClient(clientId) {
  clients.delete(clientId);

  Object.keys(clients).forEach(function (key) {
    if(clients[key].id === clientId )clients.delete(key);
  });
}

module.exports = { addClient, getClient, removeClient };
