// Requires
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

let postSchema = Schema(
  {
    title: {
      required: "Title is empty",
      type: String
    },
    content: {
      required: "Content is empty",
      type: String
    },
    user_id: {
      type: ObjectId,
      ref: 'User'
    }
  },{
    timestamps: {
      createdAt: 'created_at'
    }
  }
);

module.exports = mongoose.model('Post', postSchema);