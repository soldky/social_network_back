// Requires
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

let messageSchema = Schema(
    {
      text: {
        required: "Contain message is empty",
        type: String
      },
      from: {
        required: "Author is empty",
        type: ObjectId,
        ref: 'User'
      }
    },{
      timestamps: {
        createdAt: 'created_at'
      }
    }
);

module.exports = mongoose.model('Message', messageSchema);