// Requires
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

const Message = require('./Message.model');
const User = require('.//User.model');

let talkSchema = Schema(
  {
    contributors: [{
      type: ObjectId,
      ref: User,
      required: "Contributors is empty",
    }],
    messages: [{
      type: ObjectId,
      ref: Message
    }]
  },{
    timestamps: {
      createdAt: 'created_at'
    }
  }
);

module.exports = mongoose.model('Talk', talkSchema);