// Requires
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;

const Post = require('./Post.model');
const Talk = require('./Talk.model');
const User = require('.//User.model');

const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const jwt_sign_secret = require('../../config/config').JWT_SIGN_SECRET;
const jwt_expired = require('../../config/config').JWT_EXPIRED;

let userSchema = Schema(
    {
      name: {
          firstname: {
              type: String,
              required: "Firstname is empty"
          },
          lastname: {
              type: String,
              required: "Lastname is empty"
          },
          displayname: {
              type: String
          }
      },
      password: {
          required: "Password is empty",
          type: String
      },
      email: {
          required: "Email is empty",
          type: String,
          lowercase: true,
          trim: true,
          unique: true,
          match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
      },
      picture: {
        type: String,
        default: 'default_avatar.png'
      },
      poster: {
        type: String,
        default: 'default_poster.png'
      },
      birthday: {
          type: Date
      },
      posts: [{
        type: ObjectId,
        ref: Post
      }],
      friends: [{
        type: ObjectId,
        ref: User
      }],
      talks: [{
        type: ObjectId,
        ref: Talk
      }]
    },{
      timestamps: {
        createdAt: 'created_at'
      }
    }
);

userSchema.methods = {
	authenticate: function (password) {
		return passwordHash.verify(password, this.password);
	},

	generate_token: function () {
		return jwt.sign(
		  {
        userId: this._id,
        userEmail: this.email
      },
      jwt_sign_secret,
      {
        expiresIn: jwt_expired
      }
    );
	}
};

module.exports = mongoose.model('User', userSchema);