const jwt = require('jsonwebtoken');
const jwt_sign_secret = require('../../config/config').JWT_SIGN_SECRET;

function authentificate(authorization) {
  return getUserId(authorization);
}

function generate_password() {
  pass = "";
  for(let x = 0; x < 10; x++) {
     pass += String.fromCharCode(Math.floor(Math.random()*(122-48+1)+48));
  }
  return pass;
}

function parseAuthorization(authorization) {
  return (authorization != null) ? authorization.replace('Bearer ', '') : null;
}

function getUserId(authorization) {
  let userId = -1;
  const token = parseAuthorization(authorization);

  if(token != null) {
    const jwtToken = jwt.verify(token, jwt_sign_secret);
    if(jwtToken != null) {
      userId = jwtToken.userId;
    }
  }
  return userId;
}

module.exports = { authentificate, generate_password };