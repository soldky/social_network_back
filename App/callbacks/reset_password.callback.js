const EmailManager = require('../services/email/EmailManager');
const passwordHash = require('password-hash');

const generate_password = require('../utils/user.utils').generate_password;

function reset_password_callback(error, user, res) {
  if (error) {
    console.log(error);
    res.status(500).json({
      "text": "Erreur interne"
    });
  }
  else if(!user) {
    res.status(404).json({
      "text": "Aucun compte ne possède cet email."
    });
  }
  else {
    if (user.password = generate_password()) {
      EmailManager.send(user.email, "Réinitialisation du mot de passe", "Votre mot de passe à été réinitialisé. Voici votre nouveau mot de passe : " + user.password)
        .then(
        () => {
          user.password = passwordHash.generate(user.password);
          user.save();
          res.status(200).json({
            "text": "Réinitialisation ok."
          });
        }).catch((error) => {
          if(error.code === "EDNS") {
            res.status(500).json({
              "text": "Aucune connexion internet."
            })
          }
          else {
            res.status(error.response.status).json({
              "text": error.response.data.text
            })
          }
      });
    }
    else{
      res.status(500).json({
          "text": "Problème lors de la génération du nouveau mot de passe."
      });
    }
  }
}

module.exports = reset_password_callback;