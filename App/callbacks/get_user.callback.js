function get_user_callback(error, user, res) {
  if (error) {
    console.log(error);
    res.status(500).json({
      "text": "Erreur interne"
    });
  }
  else if(!user) {
    res.status(404).json({
      "text": "Aucun compte ne possède cet email."
    });
  }
  else {
    res.status(200).json({
      "user": user
    });
  }
}

module.exports = get_user_callback;