const User = require('../models/User.model');

function remove_one_friend_callback(error, res, user, friendId="") {

  if (error) {
    res.status(404).json({
      "text": "Aucun message ne possède cet id."
    });
  }
  else if(friendId && user) {
    User
      .findOneAndUpdate({ _id: friendId }, { $pull: { "friends": user._id } })
      .exec(function (error, user) {
        remove_one_friend_callback(error, res, user);
      });
  }
  else {
    res.status(200).json({
      "text": "Ami supprimé."
    });
  }
}

module.exports = remove_one_friend_callback;