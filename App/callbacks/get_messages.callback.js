function get_messages_callback(error, talk, res) {
  if (error) {
    res.status(404).json({
      "text": "Aucun message ne possède cet id."
    });
  }
  else {
    res.status(200).json({
      "messages": talk.messages
    });
  }
}

module.exports = get_messages_callback;