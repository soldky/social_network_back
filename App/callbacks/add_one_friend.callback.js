const User = require('../models/User.model');

function add_one_friend_callback(error, res, user, friendId="") {

  if (error) {
    res.status(404).json({
      "text": "Aucun message ne possède cet id."
    });
  }
  else if(friendId && user) {
    User
      .findOneAndUpdate({ _id: friendId }, { $push: { "friends": user._id } })
      .exec(function (error, user) {
        add_one_friend_callback(error, res, user);
      });
  }
  else {
    res.status(200).json({
      "text": "Ami ajouté."
    });
  }
}

module.exports = add_one_friend_callback;