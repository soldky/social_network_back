const login_callback = require("../callbacks/login.callback");

function register_callback(error, user, passwordUnencrypt, res) {
  if(error) {
    if (error.code === 11000) {
      res.status(409).json({
        "text": "L'adresse email est déjà utilisée."
      });
    }
    else {
      console.error(error);
      res.status(500).json({
        "text": "Erreur interne."
      });
    }
  }
  else {
    login_callback(error, user, passwordUnencrypt, res);
  }
}

module.exports = register_callback;