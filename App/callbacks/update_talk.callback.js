function update_talk_callback(error, talk, res) {
  if (error) {
    console.log(error);
    res.status(500).json({
      "text": "Erreur interne"
    });
  }
  else if(!talk) {
    res.status(400).json({
      "text": "Problème lors de la modification de la conversation."
    });
  }
  else {
    res.status(200).json({
      "talk": talk
    });
  }
}

module.exports = update_talk_callback;