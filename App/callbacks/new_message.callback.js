const Talk = require('../models/Talk.model');

// Callbacks
const update_talk_callback = require("./update_talk.callback");

function new_message_callback(message, res, talk_id) {
  if(!message) {
    res.status(400).json({
      "text": "Erreur lors de l'enregistrement du message."
    });
  }
  else {
    Talk
      .findOneAndUpdate({ _id: talk_id }, { $push: { "messages": message } })
      .exec(function (error, talk) {
        update_talk_callback(error, talk, res);
      });
  }
}

module.exports = new_message_callback;