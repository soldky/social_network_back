function login_callback(error, user, passwordUnencrypt, res) {
  if (error) {
    console.error(error);
    res.status(500).json({
      "text": "Erreur interne"
    });
  }
  else if(!user) {
    res.status(404).json({
      "text": "L'utilisateur n'existe pas"
    });
  }
  else {
    if (user.authenticate(passwordUnencrypt)) {
      res.status(200).json({
        "user": user,
        "token": user.generate_token(),
      });
    }
    else{
      res.status(403).json({
          "text": "Mot de passe incorrect"
      });
    }
  }
}

module.exports = login_callback;